import java.awt.Point;


@SuppressWarnings("serial")
public class Punto extends Point {
	private double x;
	private double y;
	
	public Punto() {
		// TODO Auto-generated constructor stub
		super();
	}
	
	public Punto(double x, double y){
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public String toString(){
		return "("+this.x +" ," +this.y+")";
	}
	
	

}
