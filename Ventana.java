import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

 
@SuppressWarnings("serial")
public class Ventana extends JFrame {
	private Mexico ventanita;
	private int selected;
	private PolygonCanvas mainPanel;

	public Ventana(){
		super();
		this.setSize(1024, 700);
		String[] opciones= {"Mapa de México","Generado desde Archivo"};
		
		this.selected = JOptionPane.showOptionDialog(null, "Selecciona una opción", "Mex", JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, opciones, opciones[0]);
		//System.out.println(selected);
		if(this.selected == 0){
			this.setTitle("México");
			this.ventanita = new Mexico(this.getWidth(),this.getHeight());
			this.add(this.ventanita,BorderLayout.CENTER);
		}
		else if (this.selected == 1){
			this.mainPanel = new PolygonCanvas(this.getWidth(),this.getHeight());
			this.setTitle(this.mainPanel.getNombreVentana());
			this.add(this.mainPanel,BorderLayout.CENTER);
		}
		this.setVisible(true);
		this.setResizable(false);
		
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	}

	public static void main(String[] args){
		@SuppressWarnings("unused")
		Ventana prueba = new Ventana();
	}
}