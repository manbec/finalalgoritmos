import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;


@SuppressWarnings("serial")
public class PolygonCanvas extends JPanel implements MouseMotionListener, MouseListener, KeyListener, Shape {

//	String pruebapol = "pollo";
	
	private Vector<Poligono> figuras;
	private Double mouseX;
	private Double mouseY;
	private int figuraSeleccionada;
	private String titulo;
	private String nombreVentana;

	public PolygonCanvas(int w, int h){
		super();
		this.setFiguraSeleccionada(-1);
		this.mouseX = 0.0;
		this.mouseY = 0.0;
		this.setPreferredSize(new Dimension(w,h));
		this.requestFocusInWindow(true);
		//this.setBackground(Color.BLUE);  //new Color(0x085fa3)
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		JFileChooser chooser = new JFileChooser();
		String folder = null;
		FileNameExtensionFilter filter = new FileNameExtensionFilter("CSV Files", "csv");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(chooser);
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			System.out.println("Archivo seleccionado: " +
					chooser.getSelectedFile().getAbsolutePath());
			folder = chooser.getSelectedFile().getAbsolutePath();
			this.setNombreVentana(folder.split("/")[folder.split("/").length-1]);
		}
		try{
			figuras = new Vector<Poligono>();
			BufferedReader lector= new BufferedReader(new FileReader(folder));
			String linea;
			String nombreTempPoligono = null;
			lector.readLine();
			StringTokenizer Tk;
			//lectura de archivo
			this.titulo = lector.readLine();
			Vector<Double> tempX = new Vector<Double>();
			Vector<Double> tempY = new Vector<Double>();
			Double[] xArray, yArray;
			Boolean xFlag = true;
			String token;
			while((linea=lector.readLine())!=null){
				Tk = new StringTokenizer(linea, ",");
				if(xFlag){
					nombreTempPoligono = Tk.nextToken();
					linea = lector.readLine();
					Tk = new StringTokenizer(linea, ",");
				}
				while	(Tk.hasMoreTokens()){
					token = Tk.nextToken();
					try{
						if(xFlag){
							tempX.add(Double.parseDouble(token));
						}
						else{
							tempY.add(Double.parseDouble(token));
						}
					}
					catch(NumberFormatException e){
						System.out.println(e);
					}
					
				}
				if(!xFlag){
					xArray = new Double[tempX.size()];
					/*for(int i=0;i<xArray.length;i++){
						System.out.print(tempX.get(i));
					}*/
					tempX.toArray(xArray);
					yArray = new Double[tempY.size()];
					tempY.toArray(yArray);
					this.figuras.add(new Poligono(xArray,yArray,nombreTempPoligono));
					tempX = new Vector<Double>();
					tempY = new Vector<Double>();
				}
				xFlag = !xFlag;
			}
			lector.close();
		}catch (FileNotFoundException e){
			System.out.println(e);
		}
		catch(IOException e){
			System.out.println(e);
		}
		catch(NumberFormatException e){
			System.out.println(e);
		}
		catch(ArrayIndexOutOfBoundsException e){
			System.out.println(e);
		}

		/*System.out.println("Imprimiendo poligonos del Mapa: " +this.titulo);
		for(int i=0; i<this.figuras.size();i++){
			this.figuras.get(i).printPoligono();
		}*/

	}
	
//	private void detectMouse(Graphics2D g, Shape shape) {
//		// TODO Auto-generated method stub
//		try{
//			if(this.contains(this.mouseX,this.mouseY)){
//		    	g.setColor(new Color(0xD3D3D3));
//		    	g.fill(shape);
//		    }
//		}
//		catch(NullPointerException r){
//			System.out.println(r);
//			
//		}
//	}
	

	public void paint(Graphics g){
		g.setColor(new Color(0x085fa3));
		g.fillRect(0, 0, 1040, 740);
		this.pintaPoligonos(g);
		//g.drawString("Mouse en X: "+this.mouseX+" Y: "+this.mouseY, 800, 40);
		//g.drawString(this.pruebapol, 800, 80);
	}

	//Pintar pol�gono //http://www.java2s.com/Code/Java/2D-Graphics-GUI/FillGeneralPath.htm
	public void pintaPoligonos(Graphics g){
		Graphics2D g2 = (Graphics2D) g;
		Shape shape = null;
		for(int i =0;i<this.figuras.size();i++){
			Poligono tempPol = this.figuras.get(i);
//			System.out.println(tempPol.getName());
			shape = new GeneralPath();
			((GeneralPath) shape).moveTo(tempPol.getXIn(0), tempPol.getYIn(0));
			for(int j =1;j<tempPol.getSize();j++){
				((GeneralPath) shape).lineTo(tempPol.getXIn(j), tempPol.getYIn(j));
//				System.out.println(tempPol.getXIn(j)+"!!"+tempPol.getYIn(j));
			}
			((GeneralPath) shape).closePath();
			g2.setPaint(new Color(0x3c7a51));
			g2.fill(shape);
			g2.setPaint(Color.WHITE);
			g2.draw(shape);
			
//			System.out.println("done");
			//if (this.drawSquares){ this.paintSquare(g, shape); }
			//		    this.detectMouse(g, shape, "Tlaxcala");
		}
		if(this.figuraSeleccionada>=0){
			g.drawString(this.figuras.elementAt(this.figuraSeleccionada).getName(), this.getWidth()-100, 20);
			shape = new GeneralPath();
			Poligono tempPol = this.figuras.elementAt(this.figuraSeleccionada);
			((GeneralPath) shape).moveTo(tempPol.getXIn(0), tempPol.getYIn(0));
			for(int j =1;j<tempPol.getSize();j++){
				((GeneralPath) shape).lineTo(tempPol.getXIn(j), tempPol.getYIn(j));
//				System.out.println(tempPol.getXIn(j)+"!!"+tempPol.getYIn(j));
			}
			((GeneralPath) shape).closePath();
			g2.setPaint(new Color(0x3c1a11));  //new Color(0x3c7a51)
			g2.fill(shape);
			g2.setPaint(Color.WHITE);
			g2.draw(shape);
		}
		
		
		
	}
//	void izqDer(int x, int y, Poligono figura){
//		double res = figura.getXIn(0)*figura.getYIn(1)-figura.getXIn(1)*figura.getYIn(0);
//		if(res<0){
//			this.pruebapol =  "izq";
//		}
//		else{
//			this.pruebapol =  "der";
//		}
//	}
	

	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		if (Desktop.isDesktopSupported()){
			try {
				String[] splitted = this.figuras.elementAt(this.figuraSeleccionada).nombre.split(" ");
				String res="";
				for (int i = 0 ; i< splitted.length; i++){
					res+=splitted[i]+"+";
				}
				String URL = "http://www.google.com/search?q="  + res;
				Desktop.getDesktop().browse(new URI(URL));
			} catch (IOException | URISyntaxException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
	

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		this.mouseX=(double) arg0.getX();
		this.mouseY=(double) arg0.getY();
		//this.izqDer(this.mouseX, this.mouseY, this.figuras.get(0));
		this.contains(this.mouseX, this.mouseY);
		this.repaint();
	}
	
	public boolean contains(Point p){
		Punto q = (Punto)p;
		boolean rev = false;
		for (int i = 0; i< this.figuras.size() && !rev; i++){
			if(this.figuras.elementAt(i).isIn(q)){
				rev = true;
				this.figuraSeleccionada = i;
				System.out.println(this.figuras.elementAt(this.figuraSeleccionada));
			}
		}
		return rev;
	}

	@Override
	public boolean contains(Point2D p) {
		// TODO Auto-generated method stub
		Punto q = (Punto) p;
		boolean rev = false;
		
		for (int i = 0; i< this.figuras.size() && !rev; i++){
			if(this.figuras.elementAt(i).isIn(q)){
				rev = true;
				this.figuraSeleccionada = i;
			}
		}
		return rev;
	}
	

	@Override
	public boolean contains(Rectangle2D r) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean contains(double x, double y) {
		// TODO Auto-generated method stub
		Punto p = new Punto(x,y);
		boolean rev = false;
		for (int i = 0; i< this.figuras.size() && !rev; i++){
			if(this.figuras.elementAt(i).isIn(p)){
				rev = true;
				this.figuraSeleccionada = i;
				System.out.println("Figura Seleccionada: "+ this.figuraSeleccionada);
//				System.out.println(this.figuras.elementAt(this.figuraSeleccionada));
			}
		}
		if (!rev){
			this.figuraSeleccionada =-1;
		}
		return rev;
	}

	@Override
	public boolean contains(double x, double y, double w, double h) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Rectangle2D getBounds2D() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PathIterator getPathIterator(AffineTransform at) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PathIterator getPathIterator(AffineTransform at, double flatness) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean intersects(Rectangle2D r) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean intersects(double x, double y, double w, double h) {
		// TODO Auto-generated method stub
		return false;
	}

	public int getFiguraSeleccionada() {
		return figuraSeleccionada;
	}

	public void setFiguraSeleccionada(int figuraSeleccionada) {
		this.figuraSeleccionada = figuraSeleccionada;
	}

	public String getNombreVentana() {
		return nombreVentana;
	}

	public void setNombreVentana(String nombreVentana) {
		this.nombreVentana = nombreVentana;
	}

}
