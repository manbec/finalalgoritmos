public class Poligono {
//	private double[] xCoords;
//	private double[] yCoords;
	private Punto[] coordenadas;
	private Punto[] mascara;
	String nombre;
	private boolean masked;
	public Poligono(double[]x, double[]y, String nombre){
		this.nombre = nombre;
//		this.xCoords = x;
//		this.yCoords = y;
		this.coordenadas = new Punto[x.length];
		for(int i = 0;i<x.length;i++){
//			tempx[i]=x[i];
//			tempy[i]=y[i];
			this.coordenadas[i] = new Punto(x[i],y[i]);
		}
		this.nombre = nombre;
		this.masked = false;
		if(this.coordenadas.length>10){
			this.masked=true;
			this.generarMascara(this.coordenadas);
		}
	}
	private void generarMascara(Punto[] coords) {
		int newlen = 10;
		Punto[] tempmask = new Punto[newlen];
		int jump = coords.length/newlen;
		for(int i =0;i<newlen;i++){
			tempmask[i] = coords[jump*i%coords.length];
			System.out.print(tempmask[i]+",");
		}
		System.out.println();
	}
	public Poligono(Double[] x, Double[] y, String nombre) {
//		double [] tempx = new double[x.length];
//		double [] tempy = new double[y.length];
		this.coordenadas = new Punto[x.length];
		for(int i = 0;i<x.length;i++){
//			tempx[i]=x[i];
//			tempy[i]=y[i];
			this.coordenadas[i] = new Punto(x[i].doubleValue(),y[i].doubleValue());
		}
		this.nombre = nombre;
//		this.xCoords = tempx;
//		this.yCoords = tempy;
	}
	
	void printPoligono(){
		System.out.println(this);
	}
	
	public String toString(){
		String s=this.nombre;
		s+=" Puntos: ";
		for(int i=0;i<this.coordenadas.length;i++){
//			s+=xCoords[i]+" ";
			s+= coordenadas[i].toString() +" \n";
		}
//		s+="Y: ";
//		for(int i=0;i<this.yCoords.length;i++){
//			s+=yCoords[i]+" ";
//		}
		return s;
	}
	int getSize(){
//		return this.xCoords.length;
		return this.coordenadas.length;
	}
	public double getXIn(int i){
//		return this.xCoords[i];
		return this.coordenadas[i].getX();
	}
	public double getYIn(int i){
//		return this.yCoords[i];
		return this.coordenadas[i].getY();
	}
	
	public Punto coordAt(int i){
		return this.coordenadas[i];
	}
	public String getName(){
		return this.nombre;
	}
	public Punto[] getCoordenadas() {
		return coordenadas;
	}
	public void setCoordenadas(Punto[] coordenadas) {
		this.coordenadas = coordenadas;
	}
	
	public boolean isIn(Punto q){
		double angle;
		double suma = 0.0;
		if(!masked){
			int n = this.coordenadas.length;
			for(int i = 0; i < n - 1; i++){
				angle = angleBetweenVectors(q, this.coordenadas[i], this.coordenadas[i+1]);
	//			System.out.println("Angle: " +angle);
				suma = suma + angle;
			}
			suma = suma + angleBetweenVectors(q, this.coordenadas[n-1], this.coordenadas[0]);
			System.out.println("Total angle: " +suma);
		}
		else{
			int n = this.mascara.length;
			for(int i = 0; i < n - 1; i++){
				angle = angleBetweenVectors(q, this.mascara[i], this.mascara[i+1]);
	//			System.out.println("Angle: " +angle);
				suma = suma + angle;
			}
			suma = suma + angleBetweenVectors(q, this.mascara[n-1], this.mascara[0]);
			System.out.println("Total angle: " +suma);
		}
		if(Math.abs(suma) >= 6.0) //0.00001
		{
			return true;
		}
		return false;
	}
	
	public double angleBetweenVectors(Punto q, Punto p0, Punto p1){
		double magQP0 = Math.sqrt(Math.pow(p0.getX()-q.getX(), 2.0)+Math.pow(p0.getY()-q.getY(), 2.0));
		double magQP1 = Math.sqrt(Math.pow(p1.getX()-q.getX(), 2.0)+Math.pow(p1.getY()-q.getY(), 2.0));
		double pp = (p0.getX()-q.getX())*(p1.getX()-q.getX()) + (p0.getY()-q.getY())*(p1.getY()-q.getY());
		double angle = Math.acos(pp/(magQP0 * magQP1));
		return angle;
	}
}
